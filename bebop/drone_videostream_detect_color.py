import sys
import numpy as np
import cv2

from bebop import Bebop
cnt = 0

def videoCallback( frame, robot=None, debug=False ):
    # define the upper and lower boundaries for a color
    # to be considered "blue"
    #redLower = np.array([0, 0, 170], dtype="uint8")
    #redUpper = np.array([80, 80, 255], dtype="uint8")

    yellowLower = np.array([0, 170, 170], dtype="uint8")
    yellowUpper = np.array([190, 255, 255], dtype="uint8")

    global  cnt
    cnt = cnt + 1
    f = open("./images/frame.h264", "wb", 0)
    f.write( frame[-1] )
    f.close()
    cap = cv2.VideoCapture("./images/frame.h264")
    ret, img = cap.read()
    cap.release()
    if ret:
        cv2.imwrite("./images/" + str(cnt) + ".jpg", img)
        # determine which pixels fall within the blue boundaries
        # and then blur the binary image
        yellow = cv2.inRange(img, yellowLower, yellowUpper)
        yellow = cv2.GaussianBlur(yellow, (3, 3), 0)

        # find contours in the image
        cnts, hierarchy = cv2.findContours(yellow.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        c = sorted(cnts, key=cv2.contourArea, reverse=True)[0]

        if cv2.contourArea(c) > 20000:
            print "YELLOW!!!!!"

        cv2.imshow('image', img)
        cv2.imshow("yellow", yellow)
        cv2.waitKey(10)

print "Connecting to drone.."
drone = Bebop( metalog=None, onlyIFrames=True )
drone.videoCbk = videoCallback
drone.videoEnable()
print "Connected."
for i in xrange(1000):
    drone.update( );

print "Battery:", drone.battery