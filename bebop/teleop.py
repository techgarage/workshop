import pygame
import sys
from commands import *
from bebop import Bebop

drive_speed = 50
turn_speed = 50

print ("Connecting to drone...");
metalog=None
drone = Bebop( metalog=metalog, onlyIFrames=True )
drone.trim()

print ("Connected.");



pygame.init()
pygame.display.set_mode((100, 100))
pygame.key.set_repeat(50)



while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
            
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                print('Forward')
                drone.update( cmd=movePCMDCmd( True, 0, 50, 0, 0 ) )
                drone.wait(0.100) 
                
            elif event.key == pygame.K_s:
                print('Backward')
                drone.update( cmd=movePCMDCmd( True, 0, -50, 0,0 ) )
                drone.wait(0.100) 
                
            elif event.key == pygame.K_a:
                print('Left')
                drone.update( cmd=movePCMDCmd( True, -50, 0, 0,0 ) )
                drone.wait(0.100)  
            elif event.key == pygame.K_d:
                print('Right')
                drone.update( cmd=movePCMDCmd( True, 50, 0, 0,0 ) )
                drone.wait(0.100) 
                
            elif event.key == pygame.K_t:
                print('Take Off');  
                drone.takeoff();
            elif event.key == pygame.K_m:
                print('Land')    
                drone.land();
            elif event.key == pygame.K_i:
                print('Up')
                drone.update( cmd=movePCMDCmd( True, 0, 0, 0, 50 ) )
                drone.wait(0.050) 
            elif event.key == pygame.K_k:
                print('Down')
                drone.update( cmd=movePCMDCmd( True, 0, 0, 0, -50 ) )
                drone.wait(0.050)
            elif event.key == pygame.K_l:
                print('Clockwise')              
                drone.update( cmd=movePCMDCmd( True, 0, 0, 50, 0 ) )
                drone.wait(0.050)
        
            elif event.key == pygame.K_j:
                print('CounterClockwise')
                drone.update( cmd=movePCMDCmd( True, 0, 0,-50, 0 ) ) 
                drone.wait(0.050)
            elif event.key == pygame.K_SPACE:
               print("Emergency landing")
               if drone.flyingState is None or drone.flyingState == 1: # taking off
                   drone.emergency()
               drone.land()
    
    
        drone.update( cmd=movePCMDCmd( False, 0, 0, 0, 0 ) )
        drone.update( cmd=None )
