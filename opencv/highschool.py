from os import listdir
from os.path import join
import math
import cv2
import cv2.cv as cv
import numpy as np
from sklearn.cluster import KMeans

import utils


def detect_shapes(img, result):
    whiteLower = np.array([160, 160, 160], dtype="uint8")
    whiteUpper = np.array ([255, 255, 255], dtype="uint8")

    gray = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2GRAY)
    tresholded = cv2.inRange(img, whiteLower, whiteUpper)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    circles = cv2.HoughCircles (blurred, cv.CV_HOUGH_GRADIENT, 1, minDist=30)
    if circles is not None:
        circles = np.uint16(np.around(circles))
        print circles
        for i in circles[0, :]:
            # draw the outer circle
            cv2.circle(img, (i[0], i[1]), i[2], (0, 0, 255), 2)
            # draw the center of the circle
            cv2.circle(img, (i[0], i[1]), 2, (0, 0, 255), 3)

            center_pixel = img[i[0], i[1]]
            circle_center_x = i[0]
            circle_center_y = i[1]
            circle_radius = i[2]
            circle_area = math.pi * i[2] * i[2]

            print "Circle  center = ({}, {}), radius = {}, area = {}".format(i[0], i[1], i[2], circle_area)
            if center_pixel[0] > 100 and center_pixel[1] > 100 and center_pixel[2] > 100:
                text = "Black Circle"
            else:
                text = "White Circle"
                # look for contours (human)
                cnts = cv2.findContours(tresholded.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                cnts = cnts[0]
                for c in cnts:
                    m = cv2.moments(c)
                    if m['m00'] <> 0:
                        # calculate center of the contour
                        cx = int(m['m10'] / m['m00'])
                        cy = int(m['m01'] / m['m00'])
                        area = cv2.contourArea(c)
                        if cy > circle_center_y + 50 and cx > circle_center_x - circle_radius and cx < circle_center_x + circle_radius and (circle_area / area) < 1.5:
                            print "Contour center = ({}, {}), area = {} ".format(cx, cy, cv2.contourArea(c))
                            cv2.drawContours(img, [c], -1, (0, 255, 0), 2)
                            cv2.putText(result, "Human", (10, 150), cv.CV_FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0));


            cv2.putText(result, text, (10, 75), cv.CV_FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0));


    cv2.imshow("b&w", tresholded)




def detect_dominant_color(image):
    pixels = image.reshape((image.shape[0] * image.shape[1], 3))
    clt = KMeans(1)
    clt.fit(pixels)
    hist = utils.centroid_histogram(clt)
    bar = utils.plot_colors(hist, clt.cluster_centers_)
    bar_color = bar[0,0]
    print bar_color
    return bar


print cv2.__version__
print np.__version__

image_path = '../../images/highschool/'
pictures = listdir(image_path)

# loop over all pictures
for f in pictures:
    # create complete file path
    jpeg_file = join(image_path, f)
    # read jpeg into numpy array
    img = cv2.imread(jpeg_file)
    # resize jpeg
    dst = cv2.resize(img, None, fx=0.20, fy=0.20, interpolation=cv2.INTER_AREA)


    result = detect_dominant_color(dst)
    detect_shapes(dst, result)
    cv2.imshow("Image", dst)
    cv2.imshow("Result", result)
    cv2.waitKey(0)
