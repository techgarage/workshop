import cv2
import cv2.cv as cv
import numpy as np


red = (0, 0, 255)
blue = (255, 0, 0)
green = (0, 255, 0)
white = (255, 255, 255)

image =  np.zeros((300, 300, 3), dtype = "uint8")
cv2.line(image, pt1=(0, 0),  pt2=(300, 300), color=red, thickness=2)
cv2.line(image, pt1=(300, 0),  pt2=(0, 300), color=blue, thickness=2)
cv2.circle(img=image, center=(150, 150), color=green, radius=30, thickness=2)
cv2.rectangle(img=image, pt1=(100, 20), pt2=(200, 100), color=white, thickness=cv.CV_FILLED)
cv2.putText(img=image, text="TechGarage", org=(100, 250), fontFace=cv.CV_FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=white);

cv2.imshow("Image", image)
cv2.waitKey(0)