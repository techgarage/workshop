import cv2
import cv2.cv as cv
import numpy as np


img = cv2.imread('../../images/highschool/IMG_20160418_191411607.jpg')
resized_image = cv2.resize(img, None, fx=0.15, fy=0.15, interpolation=cv2.INTER_AREA)
grayscale_image = cv2.cvtColor(resized_image.copy(), cv2.COLOR_BGR2GRAY)
blank_image =  np.zeros((200, 300, 3), dtype = "uint8")


cv2.imshow("Resized image", resized_image)
cv2.imshow("Grayscale image", grayscale_image)
cv2.imshow("New image", blank_image)

cv2.waitKey(0)