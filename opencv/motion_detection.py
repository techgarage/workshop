import cv2
import urllib 
import numpy as np

stream=urllib.urlopen('http://webcam1.lpl.org/axis-cgi/mjpg/video.cgi')
bytes=''
first_frame=None

while True:
    bytes+=stream.read(1024)
    a = bytes.find('\xff\xd8')
    b = bytes.find('\xff\xd9')

    #check if we found JPEG length
    if a!=-1 and b!=-1:       
	    # get jpeg bytes	
            jpg = bytes[a:b+2]
            # keep the rest in the buffer
            bytes= bytes[b+2:]         
            # load image
            img = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8),cv2.CV_LOAD_IMAGE_COLOR)   
    
            # convert image to grayscale and blur it
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (21, 21), 0)
         
            if first_frame == None:
                first_frame = gray
                cv2.imshow('Base frame', first_frame)

            # compute the absolute difference between the current frame and
            # first frame
            frameDelta = cv2.absdiff(first_frame, gray)
            thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

            # dilate the thresholded image to fill in holes, then find contours
            # on thresholded image
            thresh = cv2.dilate(thresh, None, iterations=2)
            (cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

         
            for c in cnts:
                # if the contour is too small, ignore it
                if cv2.contourArea(c) < 100:
                    continue

                # compute the bounding box for the contour, draw it on the frame,
                # and update the text
                (x, y, w, h) = cv2.boundingRect(c)
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
		
            cv2.imshow('original',img)
            cv2.imshow('delta',frameDelta)
        
            if cv2.waitKey(1) ==27:
                exit(0) 
