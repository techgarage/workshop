import cv2
import numpy as np

import cv2
import cv2.cv as cv
import numpy as np

img = cv2.imread('../../images/highschool/IMG_20160418_191432412.jpg')
resized_image = cv2.resize(img, None, fx=0.15, fy=0.15, interpolation=cv2.INTER_AREA)
grayscale_image = cv2.cvtColor(resized_image.copy(), cv2.COLOR_BGR2GRAY)

blurred = cv2.GaussianBlur(grayscale_image, (5, 5), 0)
circles = cv2.HoughCircles(blurred, cv.CV_HOUGH_GRADIENT, 1, minDist=30)

if circles is not None:
    circles = np.uint16(np.around(circles))
    print circles
    for i in circles[0, :]:
        # draw the outer circle
        cv2.circle(resized_image, (i[0], i[1]), i[2], (0, 0, 255), 2)
        # draw the center of the circle
        cv2.circle(resized_image, (i[0], i[1]), 2, (0, 0, 255), 3)

cv2.imshow("Image", resized_image)
cv2.imshow("Grayscale image", grayscale_image)

cv2.waitKey(0)
